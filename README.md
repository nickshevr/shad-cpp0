# Курс C++

Это основной репозиторий курса. Инструкции по настройке окружения можно найти [на вики](https://wiki.school.yandex.ru/shad/groups/2018/Semester1/LearningC/). Структура семинарских задач приведена [в тестовой задаче](https://gitlab.com/slon/shad-cpp0/tree/master/multiplication). Задачи типа crashme описаны [здесь](https://gitlab.com/slon/shad-cpp0/blob/master/crash_readme.md).

Перемещаться по задачам и следить за дедлайнами можно тут: https://cpp.manytask.org/
